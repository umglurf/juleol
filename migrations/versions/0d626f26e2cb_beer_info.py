# SPDX-FileCopyrightText: 2022 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""beer info

Revision ID: 0d626f26e2cb
Revises: 0ba92810188e
Create Date: 2022-08-21 17:45:11.804292

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0d626f26e2cb"
down_revision = "0ba92810188e"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "beer_info",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("image_url", sa.Text(), nullable=False),
        sa.Column("info_url", sa.Text(), nullable=False),
        sa.Column("beer_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["beer_id"],
            ["beers.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("beer_id"),
    )


def downgrade():
    op.drop_table("beer_info")
