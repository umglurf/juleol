# SPDX-FileCopyrightText: 2022 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""beer comment

Revision ID: 0ba92810188e
Revises: 8ac278dcefd6
Create Date: 2022-08-21 02:18:19.964368

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0ba92810188e"
down_revision = "8ac278dcefd6"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "beer_comment",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("private", sa.Boolean(), nullable=False, default=False),
        sa.Column("comment", sa.Text(), nullable=False),
        sa.Column("beer_id", sa.Integer(), nullable=False),
        sa.Column("participant_id", sa.Integer(), nullable=False),
        sa.Column("tasting_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["beer_id"],
            ["beers.id"],
        ),
        sa.ForeignKeyConstraint(
            ["participant_id"],
            ["participants.id"],
        ),
        sa.ForeignKeyConstraint(
            ["tasting_id"],
            ["tastings.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("tasting_id", "participant_id", "beer_id"),
    )


def downgrade():
    op.drop_table("beer_comment")
