/*
 * SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

function prevBeer() {
  if(currentBeerPos == 0) {
    currentBeerPos = beers.length - 1;
  } else {
    currentBeerPos = currentBeerPos - 1;
  };
  getBeerRating(beers[currentBeerPos]);
  getBeerComment(beers[currentBeerPos]);
  updateBeerPageList();
};

function nextBeer() {
  if(currentBeerPos == beers.length - 1) {
    currentBeerPos = 0;
  } else {
    currentBeerPos = currentBeerPos + 1;
  };
  getBeerRating(beers[currentBeerPos]);
  getBeerComment(beers[currentBeerPos]);
  updateBeerPageList();
};

function updateBeerPageList() {
  document.querySelectorAll("li.page-item").forEach(element => {
    if(element.dataset.beer !== undefined) {
      let prevBeer;
      let currentBeer;
      let nextBeer;
      if(currentBeerPos == 0) {
        prevBeer = beers[beers.length - 1];
      } else {
        prevBeer = beers[currentBeerPos - 1];
      }
      if(currentBeerPos == beers.length - 1) {
        nextBeer = beers[0];
      } else {
        nextBeer = beers[currentBeerPos + 1];
      }
      currentBeer = beers[currentBeerPos];
      let beer = element.dataset.beer;
      if(beer == prevBeer || beer == currentBeer || beer == nextBeer) {
        element.style.display = "block";
      } else {
        element.style.display = "none";
      }
      if(beer == currentBeer) {
        element.classList.add('active');
      } else {
        element.classList.remove('active');
      }
    }
  });
}

function setBeer(obj) {
  if(obj.dataset.beer !== undefined) {
    let beer = obj.dataset.beer;
    let num = beers.findIndex(function(e) {
      return Number(e) == this;
    }, beer);
    if(num >= 0 && num < beers.length) {
      currentBeerPos = num;
      getBeerRating(beers[currentBeerPos]);
      getBeerComment(beers[currentBeerPos]);
      updateBeerPageList();
    }
  }
};


function getBeerComment(beerNumber) {
  if(heat) {
    document.querySelector("div.beerComment").querySelector("h2").textContent = "Comment for beer " + (currentBeerPos + 1) + " (" + beerNumber + ")";
  } else {
    document.querySelector("div.beerComment").querySelector("h2").textContent = "Comment for beer " + beerNumber;
  }
  let commentIcon = document.querySelector('a[href="#beerCommentModal').querySelector("span");
  if(commentIcon !== undefined && commentIcon !== null) {
    commentIcon.remove();
  }
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  request.open("GET", "/comment/" + year + "/" + beerNumber);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function() {
    if(this.status === 200) {
      document.querySelector("input#comment").value = this.response.comment;
      if(this.response.private) {
        document.querySelector("button#private").setAttribute("aria-checked", "true");
      } else {
        document.querySelector("button#private").setAttribute("aria-checked", "false");
      }
      let commentIcon = document.createElement("span");
      commentIcon.className = "fa-solid fa-comment";
      document.querySelector('a[href="#beerCommentModal').append(commentIcon);
    } else if(this.status === 404) {
      document.querySelector("input#comment").value = "";
      document.querySelector("button#private").setAttribute("aria-checked", "false");
    } else {
      let msg;
      try {
        msg = this.response.error;
      } catch {
        msg = "Error getting beer comment";
      }
      addAlert(document.querySelector("div.beerComment").querySelector("h2"), getValidationErrorMessage(this.response, "Error getting beer comment"));
    }
  };
  request.onerror = function() {
      addAlert(document.querySelector("div.beerComment").querySelector("h2"), "Error getting beer comment");
  };
  request.send();
};

function getBeerRating(beerNumber) {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  request.open("GET", "/rate/" + year + "/" + beerNumber);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function() {
    if(this.status === 200) {
      if(heat) {
        document.querySelector("h2").textContent = "Rating for beer " + (currentBeerPos + 1) + " (" + beerNumber + ")";
        //document.querySelector("h5#beerCommentModalTitle").textContent = "Comment for beer " + (currentBeerPos + 1) + " (" + beerNumber + ")";
      } else {
        document.querySelector("h2").textContent = "Rating for beer " + beerNumber;
        //document.querySelector("h5#beerCommentModalTitle").textContent = "Comment for beer " + beerNumber;
      }
      for(let key of scoreTypes) {
        let input = document.querySelector("input#" + key);
        let output = document.querySelector("output[for='" + key + "']");
        if(this.response[key] === undefined || this.response[key] === null) {
          input.min = -1;
          input.value = -1;
          output.value = "Not set"
        } else {
          input.value = this.response[key];
          input.dataset.score = true;
          output.value = this.response[key];
        };
        if(input.value >= 0) {
          input.dispatchEvent(new Event("input"));
        }
        //updateOutput(input.form);
      }
    } else {
      let msg;
      try {
        msg = this.response.error;
      } catch {
        msg = "Error getting beer rating data";
      }
      addAlert(document.querySelector("h2"), msg);
    }
  };
  request.onerror = function() {
      addAlert(document.querySelector("h2"), "Error getting beer rating");
  };
  request.send();
};

function submitBeerComment() {
  let beerNumber = beers[currentBeerPos];
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let data = new Map();
  data["comment"] = document.querySelector("input#comment").value;
  data["private"] = document.querySelector("button#private").getAttribute("aria-checked") == "true";
  let request = new XMLHttpRequest();
  request.open("POST", "/comment/" + year + "/" + beerNumber);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
    } else {
      addAlert(document.querySelector("div.beerComment").querySelector("h2"), getValidationErrorMessage(this.response, "Error saving comment"));
    }
  };
  request.onerror = function() {
      addAlert(document.querySelector("div.beerComment").querySelector("h2"), "Error saving comment");
  };
  request.send(JSON.stringify(data));
};

function submitBeerScore(score_type) {
  let beerNumber = beers[currentBeerPos];
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let score_obj = document.querySelector("input#" + score_type);
  if(score_obj === undefined || score_obj.value === undefined) {
    return;
  }
  if(score_obj.checkValidity() === false) {
    score_obj.classList.add("is-invalid");
    return;
  }
  score_obj.classList.remove("is-invalid");
  let data = new Map();
  data[score_type] = Number(score_obj.value);
  let request = new XMLHttpRequest();
  request.open("PUT", "/rate/" + year + "/" + beerNumber);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(score_obj, getValidationErrorMessage(this.response, "Error updating score"));
    }
  };
  request.onerror = function() {
      addAlert(score_obj, "Error updating score");
  };
  request.send(JSON.stringify(data));
};

function swipe(e) {
  if(e.detail.directions.left) {
    nextBeer();
  } else if(e.detail.directions.right) {
    prevBeer();
  }
}

function updateOutput(obj) {
  if(obj.dataset.scoreType === undefined) {
    return;
  }

  let input = document.querySelector("input#" + obj.dataset.scoreType);
  let output = document.querySelector("output[for='" + obj.dataset.scoreType + "']");
  if(input.value >= 0) {
    input.min = 0;
    let thumbSize = 5;
    const ratio = (input.value - input.min) / (input.max - input.min);
    let amountToMove = ratio * ((input.offsetWidth - thumbSize) - thumbSize) - thumbSize;
    output.value = input.value;
    output.style.left = amountToMove + "px";
  } else {
    input.min = -1;
    input.value = -1;
    output.value = "Not set";
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
  getBeerRating(beers[currentBeerPos]);
  getBeerComment(beers[currentBeerPos]);
  updateBeerPageList();
  document.querySelectorAll('input[type="range"]').forEach(element => {
    element.addEventListener('change', function() {
      submitBeerScore(this.id);
    });
    element.addEventListener('input', function() {
      updateOutput(this.form);
    });
  });
  document.querySelector("#addBeerComment").addEventListener("click", submitBeerComment)
  document.querySelectorAll("li[data-beer]").forEach(element => element.addEventListener("click", function() {
    setBeer(element);
  }));
  document.querySelector("#prev").addEventListener("click", prevBeer);
  document.querySelector("#next").addEventListener("click", nextBeer);
  document.querySelectorAll("form").forEach(element => {
    element.addEventListener("change", function() {
      updateOutput(this);
    });
  });

  let element = document.querySelector("#top");
  SwipeListener(element);
  element.addEventListener("swipe", swipe);

  element = document.querySelector("h2");
  SwipeListener(element);
  element.addEventListener("swipe", swipe);
});
