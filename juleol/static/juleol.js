/*
 * SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

function addAlert(obj, msg) {
  let messagesElement = document.createElement("aside");
  messagesElement.className = "messages";

  let alertElement = document.createElement("div")
  alertElement.className = "error";

  let alertLabel = document.createElement("label");
  alertLabel.hidden = true;
  let alertInput = document.createElement("input");
  alertInput.hidden = true;
  alertInput.type = "checkbox";

  alertInput.addEventListener("input", (event) => {
    if(event.target.checked) {
      event.target.parentElement.parentElement.parentElement.remove();
    }
  });
  let alertCloseIcon = document.createElement("span")
  alertCloseIcon.className = "fa-solid fa-close";
  alertCloseIcon.setAttribute("aria-label", "Close");
  alertLabel.append(alertInput);
  alertLabel.append(alertCloseIcon);
  alertElement.append(alertLabel);

  let alertText = document.createElement("p");
  let alertIcon = document.createElement("span")
  alertIcon.className = "fa-solid fa-circle-exclamation";
  alertText.textContent = msg;
  alertText.prepend(alertIcon);
  alertElement.append(alertText);

  messagesElement.append(alertElement);
  obj.after(messagesElement);
  setTimeout(() => {
    messagesElement.remove();
  }, 5000);
}

function getValidationErrorMessage(error, fallbackMessage) {
  if (error === null || error.validation_error === undefined || error.validation_error.body_params === undefined) {
    return fallbackMessage;
  }
  return error.validation_error.body_params.map(e => e.msg).join(", ");

}

document.addEventListener("DOMContentLoaded", function(event) {
  document.querySelectorAll("aside.messages").forEach(e => {
    e.querySelector("input").addEventListener("input", (event) => {
      if(event.target.checked) {
        event.target.parentElement.parentElement.remove();
      }
    });
  });

  document.querySelectorAll("button.switch").forEach((element) => {
    element.addEventListener("click", (evt) => {
      const el = evt.target;

      if (el.getAttribute("aria-checked") === "true") {
        el.setAttribute("aria-checked", "false");
      } else {
        el.setAttribute("aria-checked", "true");
      }
    }, false);
  });

  function filterHeat() {
    let url = new URL(window.location);
    let heat = document.querySelector("#heatselect").value;
    if(heat == "All") {
      url.search = "";
    } else {
      url.search = "heat=" + heat;
    };
    window.location = url;
  };
  var heatselectElement = document.querySelector("#heatselect")
  if(heatselectElement) {
    heatselectElement.addEventListener("change", filterHeat);
  };

  function tableSort(tableQuery, element, index) {
    let tbody = document.querySelector(tableQuery).querySelector("tbody");
    let rows = new Array();
    tbody.querySelectorAll("tr").forEach((element) => {
      rows.push(element);
    });
    if(element.ariaSort === "none" || element.ariaSort === "descending") {
      element.ariaSort = "ascending";
      rows.sort((a, b) => {
        let a_num = parseFloat(a.children.item(index).textContent);
        let b_num = parseFloat(b.children.item(index).textContent);
        if(Number.isNaN(a_num) || Number.isNaN(b_num)) {
          element.querySelector("div").className = "fa-solid fa-arrow-up-a-z"
          return a.children.item(index).textContent.localeCompare(b.children.item(index).textContent);
        } else {
          element.querySelector("div").className = "fa-solid fa-arrow-up-1-9"
          return a_num - b_num;
        }
      });
    } else {
      element.ariaSort = "descending";
      element.querySelector("div").className = "fa-solid fa-sort-up"
      rows.sort((a, b) => {
        let a_num = parseFloat(a.children.item(index).textContent);
        let b_num = parseFloat(b.children.item(index).textContent);
        if(Number.isNaN(a_num) || Number.isNaN(b_num)) {
          element.querySelector("div").className = "fa-solid fa-arrow-down-a-z"
          return b.children.item(index).textContent.localeCompare(a.children.item(index).textContent);
        } else {
          element.querySelector("div").className = "fa-solid fa-arrow-down-1-9"
          return b_num - a_num;
        }
      });
    }
    document.querySelector(tableQuery).querySelectorAll("th").forEach(e => {
      if(e != element) {
        e.ariaSort = "none";
        let divElem = e.querySelector("div")
        if(divElem) {
          divElem.className = "fa-solid fa-sort";
        }
      }
    });

    tbody.replaceChildren();
    rows.forEach((element, index) => {
      element.querySelectorAll("td")[0].textContent = index + 1;
      tbody.append(element);
    });
  };

  var resulttable = document.querySelector("#resulttable");
  if(resulttable) {
    resulttable.querySelectorAll("th").forEach((element, index) => {
      if(index > 0) {
        element.addEventListener("click", function() {
          tableSort("#resulttable", element, index);
        });
      }
    });
  };
  var participantResulttable= document.querySelector("#participantResulttable");
  if(participantResulttable) {
    participantResulttable.querySelectorAll("th").forEach((element, index) => {
      if(index > 0) {
        element.addEventListener("click", function() {
          tableSort("#participantResulttable", element, index);
        });
      }
    });
  };
});
