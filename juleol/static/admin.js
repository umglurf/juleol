/*
 * SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

function deleteBeerInfo() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement;
  request.open("DELETE", "/admin/beer/" + this.dataset.beerId + "/info");
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      container.remove();
    } else {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating beer info"));
    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating beer info"));
  };
  request.send();
}

function deleteHeat() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement;
  let heatId = this.dataset.heatId;
  request.open("DELETE", "/admin/heat/" + heatId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      container.remove();
      document.querySelectorAll(".beerHeatSelect").forEach(e => {
        if(e.value === heatId) {
          e.value = "None";
        }
      });
    } else {
      addAlert(container, getValidationErrorMessage(this.response, "Error deleting heat"));
    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error deleting heat"));
  };
  request.send();
}

function deleteNote() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  request.open("DELETE", "/admin/note/" + this.dataset.noteId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      container.remove();
    } else {
      addAlert(container, getValidationErrorMessage(this.response, "Error deleting note"));
    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error deleting note"));
  };
  request.send();
}

function deleteParticipant(participantId) {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = document.querySelector('button[data-participant-id="' + participantId + '"]').parentElement.parentElement;
  request.open("DELETE", "/admin/" + year + "/participant/" + participantId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      container.remove();
    } else {
      addAlert(container, getValidationErrorMessage(this.response, "Error deleting participant"));
    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error deleting participant"));
  };
  request.send();
}

function deleteParticipantConfirmation() {
  let modal = document.createElement("section");
  modal.id = "confirmDeleteParticipant";
  modal.className = "modal";

  let outerClose = document.createElement("a");
  outerClose.href="#";
  outerClose.className = "modalClose";
  outerClose.tabIndex = -1;
  let outerCloseIcon = document.createElement("span");
  outerCloseIcon.append("Close confirm participant delete");
  outerClose.append(outerCloseIcon);
  modal.append(outerClose);

  let modalBody = document.createElement("div");

  let innerClose = document.createElement("a");
  innerClose.href="#";
  innerClose.className = "modalClose fa-solid fa-close";
  innerClose.hidden = true;
  let innerCloseIcon = document.createElement("span");
  innerCloseIcon.append("Close confirm participant delete");
  innerClose.append(innerCloseIcon);
  modalBody.append(innerClose);

  let header = document.createElement("header");
  let heading = document.createElement("h2");
  heading.append("Really delete participant?");
  header.append(heading);
  modalBody.append(header);

  let modalText = document.createElement("p");
  modalText.append("Are you sure you want to delete participant " + this.dataset.participantName + "? This will also delete all beer data for that participant");
  modalBody.append(modalText);

  let confirmButton = document.createElement("button");
  confirmButton.type = "button"
  confirmButton.append("Delete participant");
  confirmButton.dataset.participantId = this.dataset.participantId;
  confirmButton.addEventListener("click", (evt) => {
    evt.target.parentElement.parentElement.remove();
    deleteParticipant(evt.target.dataset.participantId);

  });
  modalBody.append(confirmButton);

  modal.append(modalBody);

  document.querySelector("body").append(modal);
  window.location = "#confirmDeleteParticipant";
}

function updateBeerInfo(obj, beerId, imageURL, infoURL) {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let data = new Map();
  data["image_url"] = imageURL;
  data["info_url"] = infoURL;
  request.open("POST", "/admin/beer/" + beerId + "/info");
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      obj.querySelectorAll("a").forEach(e => e.parentElement.remove());

      let container = document.createElement("fieldset");
      container.className = "beerImage";

      let legend = document.createElement("legend");
      legend.append("Beer image");
      container.append(legend);

      let link = document.createElement("a");
      link.href = infoURL;
      let img = document.createElement("img");
      img.src = imageURL;
      img.alt = "Picture of beer " + beerId;
      link.append(img)
      container.append(link);

      let button = document.createElement("button");
      button.type = "button";
      button.setAttribute("aria-label", "Remove image and info url");
      button.setAttribute("title", "Remove image and info url");
      button.dataset.beerId = beerId;
      button.addEventListener("click", deleteBeerInfo)
      let span = document.createElement("span");
      span.className = "fa-solid fa-close";
      button.append(span);
      container.append(button);

      obj.insertBefore(container, obj.querySelectorAll("fieldset")[0]);
    } else {
      addAlert(obj, getValidationErrorMessage(this.response, "Error updating beer info"));
    }
  };
  request.onerror = function() {
    addAlert(obj, getValidationErrorMessage(this.response, "Error updating beer info"));
  };
  request.send(JSON.stringify(data));
}

function updateBeerHeat() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let beerId = this.dataset.beerId;
  let data = new Map();
  if(this.value === 'None') {
    request.open("DELETE", "/admin/beer/" + beerId + "/heat");
  } else {
    request.open("PUT", "/admin/beer/" + beerId);
    data["heat"] = this.value;
  }
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating beer heat"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating beer heat"));
  };
  request.send(JSON.stringify(data));
}

function updateBeerName() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let data = new Map();
  data["name"] = this.value;
  request.open("PUT", "/admin/beer/" + this.dataset.beerId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating beer name"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating beer name"));
  };
  request.send(JSON.stringify(data));
}

function updateHeatName() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let data = new Map();
  data["name"] = this.value;
  request.open("PUT", "/admin/heat/" + this.dataset.heatId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating heat name"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating heat name"));
  };
  request.send(JSON.stringify(data));
}

function updateNoteName() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let data = new Map();
  data["note"] = this.value;
  request.open("PUT", "/admin/note/" + this.dataset.noteId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating note"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating note"));
  };
  request.send(JSON.stringify(data));
}

function updateParticipantEmail() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let data = new Map();
  data["email"] = this.value;
  request.open("PUT", "/admin/" + year + "/participant/" + this.dataset.participantId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating participant email"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating participant email"));
  };
  request.send(JSON.stringify(data));
}

function updateParticipantName() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  let data = new Map();
  data["name"] = this.value;
  request.open("PUT", "/admin/" + year + "/participant/" + this.dataset.participantId);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(container, getValidationErrorMessage(this.response, "Error updating participant name"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error updating participant name"));
  };
  request.send(JSON.stringify(data));
}


function searchBeerInfo() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let request = new XMLHttpRequest();
  let container = this.parentElement.parentElement;
  console.log(container);
  let beerId = this.dataset.beerId;
  let searchString = this.parentElement.querySelector("input").value;
  request.open("GET", "/admin/search?beerName=" + searchString);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status === 200) {
      // open modal
      window.location = "#beerSearch";
      let inputField = container.querySelector("input");
      let modalBody = document.querySelector("#beerSearchBody");
      modalBody.replaceChildren();
      let list = document.createElement("ul");
      this.response.results.forEach(item => {
        let listItem = document.createElement("li")

        let itemTitle = document.createElement("h3");
        itemTitle.append(item["name"]);
        listItem.append(itemTitle);

        let itemLink = document.createElement("a");
        itemLink.href = item["info_url"];
        let itemImage = document.createElement("img")
        itemImage.src = item["image_url"];
        itemImage.alt = item["name"];
        itemLink.append(itemImage)
        listItem.append(itemLink);

        let itemButton = document.createElement("button")
        itemButton.type = "button";
        itemButton.append("Use beer");
        listItem.append(itemButton);


        itemButton.onclick = function() {
          inputField.value = item["name"];
          window.location="#"
          updateBeerInfo(container, beerId, item["image_url"], item["info_url"])
          container.querySelector("input").dispatchEvent(new Event("focusout"));
        };

        list.append(listItem);
      });
      modalBody.append(list);
    } else {
      addAlert(container, getValidationErrorMessage(this.response, "Error searching for beer info"));

    }
  };
  request.onerror = function() {
    addAlert(container, getValidationErrorMessage(this.response, "Error searching for beer info"));
  };
  request.send();
}

/*
  function search_beer_info(obj, beer_id) {
    var csrf_token = $("#csrf-token")[0].dataset.token;
    input_field = $(obj).siblings("input")[0]
    search_string = input_field.value;
    $.ajax("/admin/search/" + search_string, {
      headers: {"X-CSRFToken": csrf_token},
      method: "GET",
      success: function(data, textStatus, xhr) {
        $(obj).siblings("p").hide();
        $("#beerSearchModalBody").empty()
        search_body = $("#beerSearchModalBody")[0]
        let list = document.createElement("div");
        list.className = "list-group";
        data.forEach(function(item) {
          let item_title = document.createElement("h5");
          item_title.className = "mb-1";
          item_title_link = document.createElement("a");
          item_title_link.href = item["url"];
          item_title_link.append(item["name"]);
          item_title.append(item_title_link);
          list.append(item_title);

          let item_body = document.createElement("div");
          item_body.className = "list-group-item list-group-item-action";
          item_body.onclick = function() {
            input_field.value = item["name"];
            update_beer_name($("#beer_" + beer_id)[0], beer_id);
            $("#beerSearchModal").modal("hide");
            update_beer_info(obj, beer_id, item["image"], item["url"]);
          };

          let item_img = document.createElement("img");
          item_img.src = item["image"];
          item_img.className = "beerinfosearch";
          item_body.append(item_img);

          list.append(item_body);

        });
        search_body.append(list);
        $("#beerSearchModal").modal("show");
      },
      error: function(xhr, ajaxOptions, thrownError) {
        try {
          var msg = xhr.responseJSON.error;
        } catch {
          var msg = "Error searching for beer status";
        }
        add_alert($(obj).parent(), msg);
      }
    });
  };
*/

function updateLocked() {
  let csrf_token = document.querySelector("#csrf-token").dataset.token;
  let data = new Map();
  data["locked"] = document.querySelector("button#locked").getAttribute("aria-checked") == "true";
  let request = new XMLHttpRequest();
  request.open("PUT", "/admin/tasting/" + year);
  request.setRequestHeader("X-CSRFToken", csrf_token);
  request.setRequestHeader("Content-Type", "application/json");
  request.responseType = "json";
  request.onload = function () {
    if(this.status !== 200) {
      addAlert(document.querySelector("#locked"), getValidationErrorMessage(this.response, "Error updating locked status"));
    }
  };
  request.onerror = function() {
      addAlert(document.querySelector("#locked"), getValidationErrorMessage(this.response, "Error updating locked status"));
  };
  request.send(JSON.stringify(data));
};

document.addEventListener("DOMContentLoaded", function(event) {
  document.querySelectorAll(".beerInfoSearch").forEach(element => {
    element.addEventListener("click", searchBeerInfo);
  });
  document.querySelectorAll(".beerImage").forEach(element => {
    element.querySelector("button").addEventListener("click", deleteBeerInfo);
  });

  document.querySelectorAll(".beerName").forEach(element => {
    element.addEventListener("focusout", updateBeerName);
  });

  document.querySelectorAll(".beerHeatSelect").forEach(element => {
    element.addEventListener("change", updateBeerHeat);
  });

  document.querySelectorAll(".heatName").forEach(element => {
    element.addEventListener("focusout", updateHeatName);
  });
  document.querySelectorAll(".heatDelete").forEach(element => {
    element.addEventListener("click", deleteHeat);
  });

  const lockObserver = new MutationObserver((mutationList, observer) => {
    updateLocked();
  });
  const lockedElement = document.querySelector("#locked");
  lockObserver.observe(lockedElement, {"attributes": true, "attributeFilter": ["aria-checked"]});

  document.querySelectorAll(".note").forEach(element => {
    element.addEventListener("focusout", updateNoteName);
  });
  document.querySelectorAll(".noteDelete").forEach(element => {
    element.addEventListener("click", deleteNote);
  });

  document.querySelectorAll(".participantEmail").forEach(element => {
    element.addEventListener("focusout", updateParticipantEmail);
  });
  document.querySelectorAll(".participantName").forEach(element => {
    element.addEventListener("focusout", updateParticipantName);
  });
  document.querySelectorAll(".participantDelete").forEach(element => {
    element.addEventListener("click", deleteParticipantConfirmation);
  });
})
