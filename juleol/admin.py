# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import (
    Blueprint,
    json,
    render_template,
    request,
    jsonify,
    redirect,
    url_for,
    flash,
    current_app,
)
from flask_pydantic import validate

from functools import wraps

from juleol import db
from juleol.types import (
    BeerInfo,
    BeerInfoSearchQuery,
    BeerUpdate,
    ErrorMessage,
    HeatUpdate,
    Message,
    NoteUpdate,
    ParticipantUpdate,
    Tasting,
)
from sqlalchemy import exc
from wtforms import (
    Form,
    BooleanField,
    EmailField,
    IntegerField,
    validators,
    StringField,
)
from wtforms.widgets import NumberInput

bp = Blueprint("admin", __name__)


class TastingForm(Form):
    year = IntegerField(
        "Year",
        [validators.input_required(), validators.NumberRange(2000, 2100)],
        widget=NumberInput(min=2000, max=2100),
    )
    beers = IntegerField(
        "Number of beers",
        [validators.input_required(), validators.NumberRange(1, 100)],
        widget=NumberInput(min=1, max=100),
    )


class ParticipantForm(Form):
    name = StringField("Name", [validators.input_required(), validators.Length(1, 255)])
    email = EmailField(
        "Email",
        [validators.input_required(), validators.Length(1, 255), validators.Email()],
    )


class NoteForm(Form):
    note = StringField("Note", [validators.input_required()])


class HeatForm(Form):
    name = StringField("Heat", [validators.input_required()])


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_app.config.get("admin_oauth").authorized:
            return redirect(url_for(current_app.config.get("admin_oauth_login")))
        return f(*args, **kwargs)

    return decorated_function


@bp.route("/admin/", methods=["GET", "POST"])
@login_required
def admin_index():
    form = TastingForm(request.form)
    if request.method == "POST" and form.validate():
        try:
            tasting = db.Tastings(year=form.year.data, locked=False)
            db.db.session.add(tasting)
            for i in range(1, form.beers.data + 1):
                new_beer = db.Beers(
                    tasting=tasting, number=i, name="Unrevealed {}".format(i)
                )
                db.db.session.add(new_beer)
            db.db.session.commit()
            flash("Tasting for year {} created".format(form.year.data), "success")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error creating tasting: {}".format(e))
            flash("Error creating tasting for year {}".format(form.year.data), "error")

    tastings = db.Tastings.query.all()
    return render_template("admin.html", tastings=tastings, form=form)


@bp.route("/admin/<year>", methods=["GET"])
@login_required
@validate()
def admin_year(year: int):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        flash("Invalid year", "error")
        return redirect(url_for("admin.admin_index"))

    participant_form = ParticipantForm(request.form)
    note_form = NoteForm(request.form)
    heat_form = HeatForm(request.form)
    has_search = current_app.config.get("search_plugin") is not None
    return render_template(
        "admin_year.html",
        tasting=tasting,
        participant_form=participant_form,
        note_form=note_form,
        heat_form=heat_form,
        has_search=has_search,
    )


@bp.route("/admin/tasting/<year>", methods=["PUT"])
@login_required
@validate()
def update_tasting(year: int, body: Tasting):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        return ErrorMessage(error="Invalid year"), 404

    try:
        tasting.locked = body.locked
        db.db.session.add(tasting)
        db.db.session.commit()
        return jsonify(message="Locked status updated")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating tasting: {}".format(e))
        response = jsonify(error="Error updating tasting")
        response.status_code = 500
        return response


@bp.route("/admin/<int:year>/participant", methods=["POST"])
@login_required
def new_participant(year):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        flash("Invalid year", "error")
        return redirect(url_for("admin.admin_index"))

    form = ParticipantForm(request.form)
    if form.validate():
        try:
            participant = db.Participants(
                tasting=tasting, name=form.name.data, email=form.email.data
            )
            db.db.session.add(participant)
            for beer in tasting.beers:
                look = db.ScoreLook(tasting=tasting, beer=beer, participant=participant)
                db.db.session.add(look)
                smell = db.ScoreSmell(
                    tasting=tasting, beer=beer, participant=participant
                )
                db.db.session.add(smell)
                taste = db.ScoreTaste(
                    tasting=tasting, beer=beer, participant=participant
                )
                db.db.session.add(taste)
                aftertaste = db.ScoreAftertaste(
                    tasting=tasting, beer=beer, participant=participant
                )
                db.db.session.add(aftertaste)
                xmas = db.ScoreXmas(tasting=tasting, beer=beer, participant=participant)
                db.db.session.add(xmas)
            db.db.session.commit()
            flash("Participant {} added".format(form.name.data), "success")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error creating participant: {}".format(e))
            flash("Error creating participant", "error")
    else:
        flash("Invalid form data", "error")

    return redirect("/admin/{}".format(year))


@bp.route("/admin/<int:year>/participant/<int:participant_id>", methods=["PUT"])
@login_required
@validate()
def update_participant(year, participant_id, body: ParticipantUpdate):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        return ErrorMessage(error="Invalid year"), 404

    participant = (
        db.Participants.query.filter(db.Participants.tasting == tasting)
        .filter(db.Participants.id == participant_id)
        .first()
    )
    if not participant:
        return ErrorMessage(error="Invalid participant"), 404

    if body.name is None and body.email is None:
        return ErrorMessage(error="No valid fields"), 400

    if body.email is not None:
        participant.email = body.email
    if body.name is not None:
        participant.name = body.name
    try:
        db.db.session.add(participant)
        db.db.session.commit()
        return Message(message="Participant updated")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating participant: {}".format(e))
        return ErrorMessage(error="Error updating participant"), 500


@bp.route("/admin/<int:year>/participant/<int:participant_id>", methods=["DELETE"])
@login_required
@validate()
def delete_participant(year, participant_id):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        return ErrorMessage(error="Invalid year"), 404

    participant = (
        db.Participants.query.filter(db.Participants.tasting == tasting)
        .filter(db.Participants.id == participant_id)
        .first()
    )
    if not participant:
        return ErrorMessage(error="Invalid participant"), 404

    try:
        for look in (
            db.ScoreLook.query.filter(db.ScoreLook.tasting == tasting)
            .filter(db.ScoreLook.participant == participant)
            .all()
        ):
            db.db.session.delete(look)
        for smell in (
            db.ScoreSmell.query.filter(db.ScoreSmell.tasting == tasting)
            .filter(db.ScoreSmell.participant == participant)
            .all()
        ):
            db.db.session.delete(smell)
        for taste in (
            db.ScoreTaste.query.filter(db.ScoreTaste.tasting == tasting)
            .filter(db.ScoreTaste.participant == participant)
            .all()
        ):
            db.db.session.delete(taste)
        for aftertaste in (
            db.ScoreAftertaste.query.filter(db.ScoreAftertaste.tasting == tasting)
            .filter(db.ScoreAftertaste.participant == participant)
            .all()
        ):
            db.db.session.delete(aftertaste)
        for xmas in (
            db.ScoreXmas.query.filter(db.ScoreXmas.tasting == tasting)
            .filter(db.ScoreXmas.participant == participant)
            .all()
        ):
            db.db.session.delete(xmas)
        db.db.session.delete(participant)
        db.db.session.commit()
        return Message(message="Participant deleted")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error deleting participant: {}".format(e))
        return ErrorMessage(error="Error deleting participant"), 500


@bp.route("/admin/<int:year>/heat", methods=["POST"])
@login_required
def new_heat(year):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        flash("Invalid year", "error")
        return redirect(url_for("admin.admin_index"))

    form = HeatForm(request.form)
    if form.validate():
        try:
            heat = db.Heats(tasting=tasting, name=form.name.data)
            db.db.session.add(heat)
            db.db.session.commit()
            flash("Heat added", "info")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error creating heat: {}".format(e))
            flash("Error creating heat", "error")
    else:
        flash("Invalid form data", "error")

    return redirect("/admin/{}".format(year))


@bp.route("/admin/heat/<heat_id>", methods=["GET"])
@login_required
@validate()
def get_heat(heat_id: int):
    heat = db.Heats.query.filter(db.Heats.id == heat_id).first()
    if not heat:
        return ErrorMessage(error="Invalid heat id"), 404
    return jsonify({"id": heat.id, "name": heat.name})


@bp.route("/admin/heat/<heat_id>", methods=["PUT"])
@login_required
@validate()
def update_heat(heat_id: int, body: HeatUpdate):
    heat = db.Heats.query.filter(db.Heats.id == heat_id).first()
    if not heat:
        return ErrorMessage(error="Invalid heat id"), 404

    try:
        heat.name = body.name
        db.db.session.add(heat)
        db.db.session.commit()
        return Message(message="Heat updated")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating heat: {}".format(e))
        return ErrorMessage(error="Error updating heat"), 500


@bp.route("/admin/heat/<heat_id>", methods=["DELETE"])
@login_required
@validate()
def delete_heat(heat_id: int):
    heat = db.Heats.query.filter(db.Heats.id == heat_id).first()
    if not heat:
        return ErrorMessage(error="Invalid heat id"), 404

    try:
        db.db.session.delete(heat)
        db.db.session.commit()
        return Message(message="Heat deleted")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error deleting heat: {}".format(e))
        return ErrorMessage(error="Error deleting heat"), 500


@bp.route("/admin/<int:year>/note", methods=["POST"])
@login_required
def new_note(year):
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        flash("Invalid year", "error")
        return redirect(url_for("admin.admin_index"))

    form = NoteForm(request.form)
    if form.validate():
        try:
            note = db.Notes(tasting=tasting, note=form.note.data)
            db.db.session.add(note)
            db.db.session.commit()
            flash("Note added", "info")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error creating note: {}".format(e))
            flash("Error creating note", "error")
    else:
        flash("Invalid form data", "error")

    return redirect("/admin/{}".format(year))


@bp.route("/admin/note/<note_id>", methods=["GET"])
@login_required
@validate()
def get_note(note_id: int):
    note = db.Notes.query.filter(db.Notes.id == note_id).first()
    if not note:
        return ErrorMessage(error="Invalid note id"), 404
    return jsonify({"id": note.id, "note": note.note})


@bp.route("/admin/note/<note_id>", methods=["PUT"])
@login_required
@validate()
def update_note(note_id: int, body: NoteUpdate):
    note = db.Notes.query.filter(db.Notes.id == note_id).first()
    if not note:
        return ErrorMessage(error="Invalid note id"), 404

    try:
        note.note = body.note
        db.db.session.add(note)
        db.db.session.commit()
        return Message(message="Note updated")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating note: {}".format(e))
        return ErrorMessage(error="Error updating note"), 500


@bp.route("/admin/note/<note_id>", methods=["DELETE"])
@login_required
@validate()
def delete_note(note_id: int):
    note = db.Notes.query.filter(db.Notes.id == note_id).first()
    if not note:
        return ErrorMessage(error="Invalid note id"), 404
    try:
        db.db.session.delete(note)
        db.db.session.commit()
        return Message(message="Note deleted")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error deleting note: {}".format(e))
        return ErrorMessage(error="Error deleting note"), 500


@bp.route("/admin/beer/<beer_id>", methods=["PUT"])
@login_required
@validate()
def beer(beer_id: int, body: BeerUpdate):
    beer = db.Beers.query.filter(db.Beers.id == beer_id).first()
    if not beer:
        return ErrorMessage(error="Invalid beer id"), 404
    if body.heat is not None:
        heat = (
            db.Heats.query.filter(db.Heats.tasting == beer.tasting)
            .filter(db.Heats.id == body.heat)
            .first()
        )
        if not heat:
            return ErrorMessage(error="Invalid heat"), 404
        try:
            beer.heat = heat
            db.db.session.add(beer)
            db.db.session.commit()
            return Message(message="Beer heat updated")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error updating beer: {}".format(e))
            return ErrorMessage(error="Error updating beer heat"), 500
    elif body.name is not None:
        try:
            beer.name = body.name
            db.db.session.add(beer)
            db.db.session.commit()
            return Message(message="Beer name updated")
        except exc.SQLAlchemyError as e:
            db.db.session.rollback()
            current_app.logger.error("Error updating beer: {}".format(e))
            return ErrorMessage(error="Error updating beer name"), 500
    else:
        return ErrorMessage(error="No valid fields"), 400


@bp.route("/admin/beer/<beer_id>/heat", methods=["DELETE"])
@login_required
@validate()
def beer_heat_delete(beer_id: int):
    beer = db.Beers.query.filter(db.Beers.id == beer_id).first()
    if not beer:
        return ErrorMessage(error="Invalid beer id"), 404
    try:
        beer.heat = None
        db.db.session.add(beer)
        db.db.session.commit()
        return Message(message="Beer heat deleted")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating beer: {}".format(e))
        return ErrorMessage(error="Error deleting beer heat"), 500


@bp.route("/admin/beer/<int:beer_id>/info", methods=["POST"])
@login_required
@validate()
def beer_info_add(beer_id, body: BeerInfo):
    beer = db.Beers.query.filter(db.Beers.id == beer_id).first()
    if not beer:
        return ErrorMessage(error="Invalid beer id"), 404
    try:
        if beer.info is not None:
            beer.image_url = body.image_url
            beer.info_url = body.info_url
        else:
            beer_info = db.BeerInfo(
                beer=beer,
                image_url=body.image_url,
                info_url=body.info_url,
            )
            beer.info = beer_info
        db.db.session.add(beer.info)
        db.db.session.commit()
        return Message(message="Beer info updated")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating beer info: {}".format(e))
        return ErrorMessage(error="Error updating beer info"), 500


@bp.route("/admin/beer/<beer_id>/info", methods=["DELETE"])
@login_required
@validate()
def beer_info_delete(beer_id: int):
    beer = db.Beers.query.filter(db.Beers.id == beer_id).first()
    if not beer:
        return ErrorMessage(error="Invalid beer id"), 404
    if beer.info is None:
        return ErrorMessage(error="No beer info to delete"), 404
    try:
        db.db.session.delete(beer.info)
        db.db.session.commit()
        return Message(message="Beer info deleted")
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error deleting beer info: {}".format(e))
        return ErrorMessage(error="Error deleting beer info"), 500


@bp.route("/admin/search", methods=["GET"])
@login_required
@validate()
def beer_info_search(query: BeerInfoSearchQuery):
    if current_app.config.get("search_plugin") is None:
        return jsonify([])
    return current_app.config.get("search_plugin").search(query.beerName), 200
