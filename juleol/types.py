# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List, Optional

from pydantic import field_validator, BaseModel, EmailStr, HttpUrl
from pydantic_core import PydanticCustomError


class BeerComment(BaseModel):
    comment: str
    private: bool


class BeerUpdate(BaseModel):
    name: Optional[str] = None
    heat: Optional[int] = None


class BeerInfo(BaseModel):
    name: Optional[str] = None
    image_url: HttpUrl
    info_url: HttpUrl


class BeerInfoSearchResult(BaseModel):
    results: List[BeerInfo]


class BeerInfoSearchQuery(BaseModel):
    beerName: str


class HeatUpdate(BaseModel):
    name: str


class ErrorMessage(BaseModel):
    error: str


class Message(BaseModel):
    message: str


class NoteUpdate(BaseModel):
    note: str


class ParticipantUpdate(BaseModel):
    name: Optional[str] = None
    email: Optional[EmailStr] = None


class Rating(BaseModel):
    look: Optional[int] = None
    smell: Optional[int] = None
    taste: Optional[int] = None
    aftertaste: Optional[int] = None
    xmas: Optional[int] = None

    @field_validator("look")
    @classmethod
    def validate_look(cls, v):
        if v is None:
            return v
        if not v >= 0 and v <= 3:
            raise PydanticCustomError(
                "look_invalid_value", "look must be between 0 and 3"
            )
        return v

    @field_validator("smell")
    @classmethod
    def validate_smell(cls, v):
        if v is None:
            return v
        if not (v >= 0 and v <= 3):
            raise PydanticCustomError(
                "smell_invalid_value", "smell must be between 0 and 3"
            )
        return v

    @field_validator("taste")
    @classmethod
    def validate_taste(cls, v):
        if v is None:
            return v
        if not (v >= 0 and v <= 9):
            raise PydanticCustomError(
                "taste_invalid_value", "taste must be between 0 and 9"
            )
        return v

    @field_validator("aftertaste")
    @classmethod
    def validate_aftertaste(cls, v):
        if v is None:
            return v
        if not (v >= 0 and v <= 5):
            raise PydanticCustomError(
                "aftertaste_invalid_value", "aftertaste must be between 0 and 5"
            )
        return v

    @field_validator("xmas")
    @classmethod
    def validate_xmas(cls, v):
        if v is None:
            return v
        if not (v >= 0 and v <= 3):
            raise PydanticCustomError(
                "xmas_invalid_value", "xmas must be between 0 and 3"
            )
        return v


class Tasting(BaseModel):
    locked: bool
