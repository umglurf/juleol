# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    mapped_column,
    relationship,
    registry,
)
from sqlalchemy.sql.expression import union_all


class Base(DeclarativeBase):
    registry = registry(type_annotation_map={str: String(255)})


db = SQLAlchemy(model_class=Base)


class Tastings(db.Model):
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    year: Mapped[int] = mapped_column(nullable=False, unique=True)
    locked: Mapped[bool] = mapped_column(nullable=False)
    beers: Mapped[List["Beers"]] = relationship(back_populates="tasting")
    heats: Mapped[List["Heats"]] = relationship(back_populates="tasting")
    participants: Mapped[List["Participants"]] = relationship(back_populates="tasting")
    notes: Mapped[List["Notes"]] = relationship(back_populates="tasting")
    score_tastes: Mapped[List["ScoreTaste"]] = relationship(back_populates="tasting")
    score_aftertastes: Mapped[List["ScoreAftertaste"]] = relationship(
        back_populates="tasting"
    )
    score_smells: Mapped[List["ScoreSmell"]] = relationship(back_populates="tasting")
    score_looks: Mapped[List["ScoreLook"]] = relationship(back_populates="tasting")
    score_xmases: Mapped[List["ScoreXmas"]] = relationship(back_populates="tasting")


class Notes(db.Model):
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    note: Mapped[str] = mapped_column(db.Text, nullable=False)
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="notes")


class Beers(db.Model):
    __table_args__ = (db.UniqueConstraint("number", "tasting_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    name: Mapped[str] = mapped_column(nullable=False)
    number: Mapped[int] = mapped_column(db.SmallInteger, nullable=False)
    heat_id: Mapped[int] = mapped_column(ForeignKey("heats.id"), nullable=True)
    heat: Mapped["Heats"] = relationship(back_populates="beers")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="beers")
    comments: Mapped[List["BeerComment"]] = relationship(back_populates="beer")
    info: Mapped["BeerInfo"] = relationship(back_populates="beer", uselist=False)
    score_tastes: Mapped[List["ScoreTaste"]] = relationship(back_populates="beer")
    score_aftertastes: Mapped[List["ScoreAftertaste"]] = relationship(
        back_populates="beer"
    )
    score_smells: Mapped[List["ScoreSmell"]] = relationship(back_populates="beer")
    score_looks: Mapped[List["ScoreLook"]] = relationship(back_populates="beer")
    score_xmases: Mapped[List["ScoreXmas"]] = relationship(back_populates="beer")


class BeerComment(db.Model):
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    comment: Mapped[str] = mapped_column(db.Text, nullable=True)
    private: Mapped[bool] = mapped_column(nullable=False)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="comments")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(back_populates="comments")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship()


class BeerInfo(db.Model):
    __table_args__ = (db.UniqueConstraint("beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    image_url: Mapped[str] = mapped_column(db.Text, nullable=True)
    info_url: Mapped[str] = mapped_column(db.Text, nullable=False)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer = db.relationship("Beers", back_populates="info", uselist=False)
    beer: Mapped["Beers"] = relationship(back_populates="info")


class Heats(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    name: Mapped[str] = mapped_column(nullable=False)
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="heats")
    beers: Mapped[List["Beers"]] = relationship(back_populates="heat")


class Participants(db.Model, UserMixin):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    name: Mapped[str] = mapped_column(nullable=False)
    email: Mapped[str] = mapped_column(nullable=False)
    comments: Mapped[List["BeerComment"]] = relationship(back_populates="participant")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="participants")
    score_tastes: Mapped[List["ScoreTaste"]] = relationship(
        back_populates="participant"
    )
    score_aftertastes: Mapped[List["ScoreAftertaste"]] = relationship(
        back_populates="participant"
    )
    score_smells: Mapped[List["ScoreSmell"]] = relationship(
        back_populates="participant"
    )
    score_looks: Mapped[List["ScoreLook"]] = relationship(back_populates="participant")
    score_xmases: Mapped[List["ScoreXmas"]] = relationship(back_populates="participant")

    def is_authenticated(self):
        return current_app.config.get("user_oauth").authorized


class ScoreTaste(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)

    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    score: Mapped[int] = mapped_column(db.SmallInteger, nullable=True)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="score_tastes")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(back_populates="score_tastes")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="score_tastes")


class ScoreAftertaste(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    score: Mapped[int] = mapped_column(db.SmallInteger, nullable=True)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="score_aftertastes")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(
        back_populates="score_aftertastes"
    )
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="score_aftertastes")


class ScoreSmell(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    score: Mapped[int] = mapped_column(db.SmallInteger, nullable=True)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="score_smells")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(back_populates="score_smells")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="score_smells")


class ScoreLook(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    score: Mapped[int] = mapped_column(db.SmallInteger, nullable=True)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="score_looks")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(back_populates="score_looks")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="score_looks")


class ScoreXmas(db.Model):
    __table_args__ = (db.UniqueConstraint("name", "tasting_id"),)
    __table_args__ = (db.UniqueConstraint("tasting_id", "participant_id", "beer_id"),)
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    score: Mapped[int] = mapped_column(db.SmallInteger, nullable=True)
    beer_id: Mapped[int] = mapped_column(ForeignKey("beers.id"), nullable=False)
    beer: Mapped["Beers"] = relationship(back_populates="score_xmases")
    participant_id: Mapped[int] = mapped_column(
        ForeignKey("participants.id"), nullable=False
    )
    participant: Mapped["Participants"] = relationship(back_populates="score_xmases")
    tasting_id: Mapped[int] = mapped_column(ForeignKey("tastings.id"), nullable=False)
    tasting: Mapped["Tastings"] = relationship(back_populates="score_xmases")


def get_beer_scores(tasting):
    s1 = db.session.query(
        Beers.id.label("beer_id"),
        Beers.tasting_id.label("tasting_id"),
        Beers.number.label("number"),
        ScoreLook.score.label("score"),
        ScoreLook.participant_id.label("participant_id"),
    ).join(ScoreLook)
    s2 = db.session.query(
        Beers.id.label("beer_id"),
        Beers.tasting_id.label("tasting_id"),
        Beers.number.label("number"),
        ScoreSmell.score.label("score"),
        ScoreSmell.participant_id.label("participant_id"),
    ).join(ScoreSmell)
    s3 = db.session.query(
        Beers.id.label("beer_id"),
        Beers.tasting_id.label("tasting_id"),
        Beers.number.label("number"),
        ScoreTaste.score.label("score"),
        ScoreTaste.participant_id.label("participant_id"),
    ).join(ScoreTaste)
    s4 = db.session.query(
        Beers.id.label("beer_id"),
        Beers.tasting_id.label("tasting_id"),
        Beers.number.label("number"),
        ScoreAftertaste.score.label("score"),
        ScoreAftertaste.participant_id.label("participant_id"),
    ).join(ScoreAftertaste)
    s5 = db.session.query(
        Beers.id.label("beer_id"),
        Beers.tasting_id.label("tasting_id"),
        Beers.number.label("number"),
        ScoreXmas.score.label("score"),
        ScoreXmas.participant_id.label("participant_id"),
    ).join(ScoreXmas)
    s = union_all(s1, s2, s3, s4, s5).alias("s")
    scores = (
        db.session.query(
            db.func.sum(s.c.score).label("sum"),
            s.c.beer_id.label("beer_id"),
            Participants.id.label("participant_id"),
        )
        .join(Participants, s.c.participant_id == Participants.id)
        .filter(s.c.tasting_id == tasting.id)
        .group_by(Participants.id, s.c.beer_id, s.c.tasting_id)
        .subquery()
    )
    details = {}
    for beer in Beers.query.filter(Beers.tasting_id == tasting.id).all():
        details[beer.number] = (
            db.session.query(scores.c.sum, Participants.name)
            .join(Participants, Participants.id == scores.c.participant_id)
            .join(Beers, Beers.id == scores.c.beer_id)
            .filter(Beers.id == beer.id)
            .all()
        )
    totals = (
        db.session.query(
            db.func.sum(scores.c.sum).over(partition_by=scores.c.beer_id).label("sum"),
            db.func.avg(scores.c.sum).over(partition_by=scores.c.beer_id).label("avg"),
            db.func.std(scores.c.sum).over(partition_by=scores.c.beer_id).label("std"),
            db.func.percentile_cont(0.5)
            .within_group(scores.c.sum)
            .over(partition_by=scores.c.beer_id)
            .label("median"),
            Beers.id.label("beer_id"),
            Beers.heat_id.label("heat_id"),
            Beers.name.label("name"),
            Beers.number.label("number"),
        )
        .join(Beers, Beers.id == scores.c.beer_id)
        .subquery()
    )

    return {
        "totals": db.session.query(
            totals,
            Heats.id.label("heat_id"),
            Heats.name.label("heat_name"),
        )
        .join(
            Heats,
            db.func.coalesce(totals.c.heat_id, "") == db.func.coalesce(Heats.id, ""),
            isouter=True,
        )
        .group_by(totals.c.beer_id)
        .all(),
        "details": details,
    }


def participant_scores(participant):
    scores = (
        db.session.query(
            Beers.number.label("number"),
            Beers.name.label("name"),
            Heats.id.label("heat_id"),
            Heats.name.label("heat_name"),
            ScoreLook.score.label("look"),
            ScoreSmell.score.label("smell"),
            ScoreTaste.score.label("taste"),
            ScoreAftertaste.score.label("aftertaste"),
            ScoreXmas.score.label("xmas"),
        )
        .join(
            Heats,
            db.func.coalesce(Beers.heat_id, "") == db.func.coalesce(Heats.id, ""),
            isouter=True,
        )
        .join(ScoreLook)
        .join(ScoreSmell)
        .join(ScoreTaste)
        .join(ScoreAftertaste)
        .join(ScoreXmas)
        .filter(ScoreLook.participant_id == participant.id)
        .filter(ScoreSmell.participant_id == participant.id)
        .filter(ScoreTaste.participant_id == participant.id)
        .filter(ScoreAftertaste.participant_id == participant.id)
        .filter(ScoreXmas.participant_id == participant.id)
        .order_by(Beers.number)
        .all()
    )
    return scores
