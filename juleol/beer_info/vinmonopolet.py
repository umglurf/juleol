# SPDX-FileCopyrightText: 2022 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from juleol.types import BeerInfoSearchResult, BeerInfo
import requests


class BeerInfoVinmonopolet:
    def __init__(self, subscription_key):
        self.session = requests.Session()
        self.session.headers.update({"Ocp-Apim-Subscription-Key": subscription_key})

    def search(self, search_string):
        params = {"productShortNameContains": search_string.replace(" ", "_")}
        resp = self.session.get(
            "https://apis.vinmonopolet.no/products/v0/details-normal", params=params
        )
        resp.raise_for_status()
        items = []
        for item in resp.json():
            product_id = item["basic"]["productId"]
            items.append(
                BeerInfo(
                    name=item["basic"]["productShortName"],
                    image_url=f"https://bilder.vinmonopolet.no/cache/515x515-0/{product_id}-1.jpg",
                    info_url=f"https://www.vinmonopolet.no/p/{product_id}",
                )
            )
        return BeerInfoSearchResult(results=items)
