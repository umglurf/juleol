# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import (
    Blueprint,
    render_template,
    request,
    session,
    jsonify,
    redirect,
    url_for,
    flash,
    current_app,
)
from flask_dance.consumer import oauth_authorized
from flask_login import current_user, login_required, login_user, logout_user
from flask_pydantic import validate
import oauthlib.oauth2.rfc6749.errors
from juleol import db
from juleol.types import *
import re
from sqlalchemy import exc
from sqlalchemy.orm import joinedload
from wtforms import (
    Form,
    validators,
    SelectField,
)

bp = Blueprint("view", __name__)


class LoginForm(Form):
    year = SelectField("Year", [validators.input_required()], coerce=str)


@oauth_authorized.connect
def logged_in(blueprint, token):
    if not token:
        flash("Failed to log in", "error")
        return False

    blueprint.token = token
    if blueprint == current_app.config.get("admin_bp"):
        return True

    if not "participant_year" in session:
        return False

    try:
        resp = blueprint.session.get(current_app.config["user_info_path"])
        print(resp)
        if not resp.ok:
            flash("Error getting userdata", "error")
            return False
        email = resp.json()["email"]
    except oauthlib.oauth2.rfc6749.errors.TokenExpiredError:
        return redirect(url_for(current_app.config.get("user_oauth_login")))

    tasting = db.Tastings.query.filter(
        db.Tastings.year == session["participant_year"]
    ).first()
    participant = (
        db.Participants.query.filter(db.Participants.tasting == tasting)
        .filter(db.Participants.email == email)
        .first()
    )
    if participant:
        login_user(participant)
        flash("Login successfull", "success")
    else:
        flash(
            "No user with email {} registered for year {}".format(
                email, session["participant_year"]
            ),
            "error",
        )
        return False


@bp.route("/login", methods=["POST"])
def login():
    form = LoginForm(request.form)
    form.year.choices = [(t.year, t.year) for t in db.Tastings.query.all()]
    if not form.validate():
        flash(f"Invalid year", "error")
        return redirect(url_for("view.index"))

    session["participant_year"] = form.year.data
    return redirect(url_for(current_app.config.get("user_oauth_login")))


@bp.route("/logout", methods=["GET"])
def logout():
    session_delete_ok = True
    if current_app.config.get("user_oauth").authorized:
        if current_app.config.get("USER_OAUTH_PROVIDER", "google") == "google":
            token = current_app.blueprints["google"].token["access_token"]
            try:
                resp = current_app.config.get("user_oauth").post(
                    "https://accounts.google.com/o/oauth2/revoke",
                    params={"token": token},
                    headers={"Content-Type": "application/x-www-form-urlencoded"},
                )
                session_delete_ok = resp.ok
                if not resp.ok:
                    current_app.logger.error(
                        "Logout failure, response from google: {}".format(resp.text)
                    )
                else:
                    del current_app.blueprints["google"].token
            except oauthlib.oauth2.rfc6749.errors.TokenExpiredError:
                # if token has expired, there is no need to revoke it
                pass

    if session_delete_ok:
        flash("Logout successfull", "success")
        session.pop("participant_year", None)
        logout_user()
    else:
        flash("Failed to log out", "error")

    return redirect(url_for("view.index"))


@bp.route("/", methods=["GET"])
def index():
    login_form = LoginForm(request.form)
    login_form.year.choices = [("", "Select year")] + [
        (t.year, t.year) for t in db.Tastings.query.all()
    ]
    login_form.year.data = login_form.year.choices[0][0]
    tastings = db.Tastings.query.all()
    return render_template(
        "index.html",
        tastings=tastings,
        current_user=current_user,
        login_form=login_form,
    )


@bp.route("/comment/<year>/<beer_number>", methods=["GET"])
@validate()
@login_required
def get_beer_comment(year: int, beer_number: int):
    try:
        beer = _validate_year_and_beer(year, beer_number)
    except ValueError as e:
        return ErrorMessage(error=str(e)), 400

    if request.method == "GET":
        comment = (
            db.BeerComment.query.filter(db.BeerComment.participant == current_user)
            .filter(db.BeerComment.beer == beer)
            .first()
        )
        if comment:
            data = {"comment": comment.comment, "private": comment.private}

            return jsonify(data)
        else:
            response = jsonify({})
            response.status_code = 404
            return response


@bp.route("/comment/<year>/<beer_number>", methods=["POST"])
@login_required
@validate()
def add_beer_comment(year: int, beer_number: int, body: BeerComment):
    try:
        beer = _validate_year_and_beer(year, beer_number)
    except ValueError as e:
        return ErrorMessage(error=str(e)), 400

    if beer.tasting.locked:
        return ErrorMessage(error="Tasting is locked"), 403

    try:
        comment = (
            db.BeerComment.query.filter(db.BeerComment.participant == current_user)
            .filter(db.BeerComment.beer == beer)
            .first()
        )
        if comment:
            comment.comment = body.comment
            comment.private = body.private
        else:
            comment = db.BeerComment(
                tasting=beer.tasting,
                beer=beer,
                participant=current_user,
                comment=body.comment,
                private=body.private,
            )
        db.db.session.add(comment)
        db.db.session.commit()
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating beer comment: {}".format(e))
        return ErrorMessage(error="Error updating beer comment"), 500

    return Message(message="Data updated")


@bp.route("/result/<int:year>")
def result(year):
    login_form = LoginForm(request.form)
    login_form.year.choices = [("", "Select year")] + [
        (t.year, t.year) for t in db.Tastings.query.all()
    ]
    login_form.year.data = login_form.year.choices[-1][0]
    tasting = db.Tastings.query.filter(db.Tastings.year == year).first()
    if not tasting:
        flash("Invalid year", "error")
        return redirect(url_for("view.index"))

    beer_scores = db.get_beer_scores(tasting)
    heat = request.args.get("heat")
    if heat is not None and re.match("^[0-9]{1,2}$", heat):
        heat = int(heat)
        beer_scores["totals"] = [s for s in beer_scores["totals"] if s.heat_id == heat]
    else:
        heat = None
    participants = db.Participants.query.filter(
        db.Participants.tasting_id == tasting.id
    ).all()

    if request.headers.get("Content-Type", "") == "application/json":
        result = {}
        result["beer_scores"] = {}
        for score in beer_scores["totals"]:
            s = int(score.sum) if score.sum else None
            a = float(score.avg) if score.avg else None
            d = float(score.std) if score.std else None
            m = float(score.median) if score.median else None
            result["beer_scores"][score.number] = {
                "name": score.name,
                "sum": s,
                "average": a,
                "stddev": d,
                "median": m,
            }
        result["participants"] = {}
        for participant in participants:
            result["participants"][participant.id] = {"name": participant.name}
        return jsonify(result)
    else:
        beers = {}
        for beer in (
            db.Beers.query.filter(db.Beers.tasting_id == tasting.id)
            .options(joinedload(db.Beers.comments))
            .options(joinedload(db.Beers.info))
            .all()
        ):
            beers[beer.number] = beer
        return render_template(
            "result.html",
            beer_scores=beer_scores,
            beers=beers,
            tasting=tasting,
            participants=participants,
            heat=heat,
            current_user=current_user,
            login_form=login_form,
        )


@bp.route("/result/<int:year>/<int:participant_id>")
def participant_result(year, participant_id):
    login_form = LoginForm(request.form)
    login_form.year.choices = [("", "Select year")] + [
        (t.year, t.year) for t in db.Tastings.query.all()
    ]
    login_form.year.data = login_form.year.choices[-1][0]
    participant = (
        db.Participants.query.join(db.Tastings)
        .filter(db.Participants.id == participant_id)
        .filter(db.Tastings.year == year)
        .one()
    )
    if not participant:
        flash("Invalid participant", "error")
        return redirect(url_for("view.index"))

    scores = db.participant_scores(participant)
    heat = request.args.get("heat")
    beers = {}
    for beer in (
        db.Beers.query.filter(db.Beers.tasting_id == participant.tasting.id)
        .options(joinedload(db.Beers.comments))
        .options(joinedload(db.Beers.info))
        .all()
    ):
        beers[beer.number] = beer
    if heat is not None and re.match("^[0-9]{1,2}$", heat):
        heat = int(heat)
        scores = [s for s in scores if s.heat_id == heat]
    else:
        heat = None
    return render_template(
        "participant_result.html",
        participant=participant,
        scores=scores,
        beers=beers,
        heat=heat,
        current_user=current_user,
        login_form=login_form,
    )


@bp.route("/rate/<int:year>", methods=["GET"])
@login_required
def rate(year):
    if not current_user.tasting.year == year:
        flash("Invalid year for this user", "error")
        return redirect(url_for("view.index"))
    heat = request.args.get("heat")
    if heat is not None and re.match("^[0-9]{1,2}$", heat):
        heat = int(heat)
    else:
        heat = None
    return render_template("rate.html", heat=heat)


@bp.route("/rate/<year>/<beer_number>", methods=["GET"])
@login_required
@validate()
def get_beer_rate(year: int, beer_number: int):
    try:
        beer = _validate_year_and_beer(year, beer_number)
    except ValueError as e:
        return ErrorMessage(error=str(e)), 400

    if request.method == "GET":
        taste = (
            db.ScoreTaste.query.filter(db.ScoreTaste.participant == current_user)
            .filter(db.ScoreTaste.beer == beer)
            .first()
        )
        aftertaste = (
            db.ScoreAftertaste.query.filter(
                db.ScoreAftertaste.participant == current_user
            )
            .filter(db.ScoreAftertaste.beer == beer)
            .first()
        )
        look = (
            db.ScoreLook.query.filter(db.ScoreLook.participant == current_user)
            .filter(db.ScoreLook.beer == beer)
            .first()
        )
        smell = (
            db.ScoreSmell.query.filter(db.ScoreSmell.participant == current_user)
            .filter(db.ScoreSmell.beer == beer)
            .first()
        )
        xmas = (
            db.ScoreXmas.query.filter(db.ScoreXmas.participant == current_user)
            .filter(db.ScoreXmas.beer == beer)
            .first()
        )

        return Rating(
            taste=taste.score,
            aftertaste=aftertaste.score,
            look=look.score,
            smell=smell.score,
            xmas=xmas.score,
        )


@bp.route("/rate/<year>/<beer_number>", methods=["PUT"])
@login_required
@validate()
def rate_beer(year: int, beer_number: int, body: Rating):
    try:
        beer = _validate_year_and_beer(year, beer_number)
    except ValueError as e:
        return ErrorMessage(error=str(e)), 400

    if beer.tasting.locked:
        return ErrorMessage(error="Tasting is locked"), 403

    try:
        if body.look is not None:
            look = (
                db.ScoreLook.query.filter(db.ScoreLook.participant == current_user)
                .filter(db.ScoreLook.beer == beer)
                .first()
            )
            look.score = body.look
            db.db.session.add(look)
        if body.smell is not None:
            smell = (
                db.ScoreSmell.query.filter(db.ScoreSmell.participant == current_user)
                .filter(db.ScoreSmell.beer == beer)
                .first()
            )
            smell.score = body.smell
            db.db.session.add(smell)
        if body.taste is not None:
            taste = (
                db.ScoreTaste.query.filter(db.ScoreTaste.participant == current_user)
                .filter(db.ScoreTaste.beer == beer)
                .first()
            )
            taste.score = body.taste
            db.db.session.add(taste)
        if body.aftertaste is not None:
            aftertaste = (
                db.ScoreAftertaste.query.filter(
                    db.ScoreAftertaste.participant == current_user
                )
                .filter(db.ScoreAftertaste.beer == beer)
                .first()
            )
            aftertaste.score = body.aftertaste
            db.db.session.add(aftertaste)
        if body.xmas is not None:
            xmas = (
                db.ScoreXmas.query.filter(db.ScoreXmas.participant == current_user)
                .filter(db.ScoreXmas.beer == beer)
                .first()
            )
            xmas.score = body.xmas
            db.db.session.add(xmas)
        db.db.session.commit()
    except exc.SQLAlchemyError as e:
        db.db.session.rollback()
        current_app.logger.error("Error updating scores: {}".format(e))
        return ErrorMessage(error="Error updating scores"), 500

    return Message(message="Data updated")


def _validate_year_and_beer(year, beer_number):
    if not current_user.tasting.year == year:
        raise ValueError("Invalid year for this user")

    beer = (
        db.Beers.query.filter(db.Beers.tasting == current_user.tasting)
        .filter(db.Beers.number == beer_number)
        .first()
    )
    if not beer:
        raise ValueError("Invalid beer")

    return beer
