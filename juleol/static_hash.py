# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import cache
from hashlib import sha256
from pathlib import Path
from os import environ
import re
from flask import (
    Blueprint,
    current_app,
    send_file,
)
from werkzeug.routing import BuildError

bp = Blueprint("static", __name__)


def static_cache(func):
    if "FLASK_DEBUG" in environ:
        return func
    else:
        return cache(func)


@static_cache
def static_hash_url(filename):
    current_app.logger.debug("Calculating hash for %s", filename)
    base_file = Path(filename)
    file = Path(current_app.root_path) / Path("static") / base_file
    if not file.exists():
        raise BuildError(
            "static", values={"filename": filename}, method="static_hash_url"
        )
    m = sha256()
    m.update(file.read_bytes())
    base_url = "/static/" + "/".join(base_file.parts[0:-1])
    if not base_url.endswith("/"):
        base_url += "/"
    return f"{base_url}{base_file.stem}.{m.hexdigest()}{base_file.suffix}"


@static_cache
def get_file_name(static_hash_filename):
    print(static_hash_filename)
    m = re.match("^(.+)[.][a-f0-9]{64}[.]([^.]+)$", static_hash_filename)
    if m is None:
        return f"static/{static_hash_filename}", current_app.config.get(
            "STATIC_FILE_MAX_AGE", None
        )
    else:
        return f"static/{m.group(1)}.{m.group(2)}", current_app.config.get(
            "STATIC_FILE_HASHED_MAX_AGE", 86400 * 30
        )


@bp.route("/static/<path:static_hash_filename>")
def send_static_hash_file(static_hash_filename):
    filename, max_age = get_file_name(static_hash_filename)
    return send_file(filename, max_age=max_age)
