# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-or-later

######################################
# BASE IMAGE
######################################
FROM docker.io/python:3.13.2-slim-bullseye AS base

RUN export DEBIAN_FRONTEND noninteractive \
    && apt-get update \
    && apt-get install -y dumb-init \
    && apt-get clean -y \
    && find /var/lib/apt/lists -delete

RUN groupadd -g 1000 app && useradd -u 1000 -g app -m app

USER 1000
WORKDIR /app
ENV PATH /home/app/.local/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

######################################
# REQUIREMENTS IMAGE
######################################
FROM base AS requirements

# renovate: datasource=pypi depName=uv
ENV UV_VERSION 0.6.5

RUN pip3 install --no-cache-dir --user "uv==${UV_VERSION}"

COPY pyproject.toml uv.lock /app/

######################################
# PACKAGE-CACHE IMAGE
######################################
FROM requirements as package-cache

RUN uv sync --no-dev

FROM requirements as package-cache-dev

RUN uv sync --dev

######################################
# PACKAGE-CACHE-DEV IMAGE
######################################
FROM package-cache-dev as test

WORKDIR /app

COPY juleol/ /app/juleol/
COPY tests/ /app/tests/
RUN .venv/bin/pytest

######################################
# PROD IMAGE
######################################
FROM base as prod

LABEL org.opencontainers.image.source="https://codeberg.org/umglurf/juleol"

USER 1000

COPY --from=package-cache /home/app/.local/ /home/app/.local/
COPY --from=package-cache /app/ /app/
COPY run /bin/run
COPY wscgi.py /app/wscgi.py
COPY juleol/ /app/juleol/
COPY migrations/ /app/migrations/

ENV LC_ALL C.UTF-8
ENV PYTHONUNBUFFERED 1

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/bin/run"]
